const array1 = [1, 2, 3];
const array2 = [4, 5, 6];
const array3 = [7, 8, 9];
const string = '🌻 Good morning.';

const result = array1.concat(array2, array3, string);
// const result = [...array1, ...array2, ...array3, string];

console.log(result);

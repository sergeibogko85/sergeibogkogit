const array = [1, 2, 3, [4, 5]];
// const array = [1, 2, 3, [4, 5, [6, 7, [8, 9]]]];
// const array = [1, 2, , 3, [4, 5]];

const result = array.flat();
// const result = array.flat(1);
// const result = array.flat(Infinity);

console.log(result);

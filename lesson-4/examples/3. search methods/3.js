const array = ['Good morning.', 'Good evening.', 'Good bye.', 'Hello.'];

const result = array.findIndex(function (element) {
    return element === 'Good bye.';
});

console.log(result);

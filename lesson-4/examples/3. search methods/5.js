const array = ['Good morning.', 'Good evening.', 'Good bye.', 'Hello.'];

const includes = function (array, targetElement) {
    const index = array.findIndex(function (element) {
        return element === targetElement;
    });

    return index === -1 ? false : true;
};

const result = includes(array, 'Good bye!');

console.log(result);

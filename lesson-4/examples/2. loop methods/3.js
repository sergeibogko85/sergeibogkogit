const numbers = [1, -1, 2, -2, 3];

const positiveNumbers = numbers.filter(
    function(number /*, index, arrayRef */) {
        return number > 0;
    },
);

console.log(numbers);
console.log(positiveNumbers);

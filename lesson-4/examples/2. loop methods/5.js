const message = 'Good morning.';
const splittedMessage = message.split('');
// const splittedMessage = message.split('').reverse();

console.log(splittedMessage);

const result = splittedMessage.reduce(function(sum, current) {
    return sum + current;
}, '');

// const result = splittedMessage.reduceRight(function(sum, current) {
//     return sum + current;
// }, '');

console.log(result);

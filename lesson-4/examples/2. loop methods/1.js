const array = ['Oscar', 'Walter'];

// callback
array.forEach(function(element, index, arrayRef) {
    console.log(`${index}:`, element, arrayRef, this);
});

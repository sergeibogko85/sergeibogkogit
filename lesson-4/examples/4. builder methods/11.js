/**
 * Маассивоподобный объект — это объект, который обладает
 * числовыми ключами, а также свойством length, которое описывает
 * количество элементов, которое содержится в объекте.
 */
const arrayLikeObject = {
    0: 'Good morning!',
    1: [11, 12, 13],
    length: 2,
};
const array = Array.from(arrayLikeObject );

console.log(array);

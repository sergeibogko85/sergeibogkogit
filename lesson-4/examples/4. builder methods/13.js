/**
 * Все инородные свойства массивоподобного объекта будут проигнорированы.
 */
const object = {
    name: 'Oscar',
    job: 'Developer',
    1: 'Good morning!',
    2: 1,
    length: 2,
};
const array = Array.from(object);

console.log(array);

/**
 * Обычный объект метод Array.from проигнорирует.
 */
const object = { name: '', age: 21 };
const array = Array.from(object);

console.log(array);

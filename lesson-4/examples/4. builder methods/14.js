// Array.from + map - модификация "на лету"
const array = Array.from([10, 11, 12], function (element) {
    console.log(element);

    return element * 2;
});

console.log(array);
